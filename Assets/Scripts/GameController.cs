﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    private SoundManager soundManager;

    private void Start() {
        soundManager = SoundManager.Instance;
    }

    public void Options() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        GameManager.Instance.ChangeGameState(GameScreens.Options);
    }

    public void Back() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        GameManager.Instance.GoBack();
    } 

    public void GamePause() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        GameManager.Instance.ChangeGameState(GameScreens.Pause);
    }

    public void ResumeGame() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        GameManager.Instance.ChangeGameState(GameScreens.InGame);
    }

    public void ResetGame() {
		GameManager.Instance.QuitGame();
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        //GameManager.Instance.ResetMatch();
    }

    public void QuitGame() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        //GameManager.Instance.QuitGame();
		Login Udata = new Login();
        Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
        StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
        StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "19", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Application.Quit();
    }
}


