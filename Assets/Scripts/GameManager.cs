﻿using com.shephertz.app42.gaming.multiplayer.client;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
//using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {


    [SerializeField] private Sprite blueFlagSprite;
    [SerializeField] private Sprite redFlagSprite;

    private bool isPcPlaying;
    private bool isPaused;
    private bool isGameOver;
    private bool isDescriptionOpen;
    public static GameSide CURRENT_TURN;
    private GameSide pcSide = GameSide.RightSide;
    private int totalSoldiersLocalSide, totalSoldiersEnemySide;
    private GameScreens prevScreen, currentScreen;
    private GameObject cloudInfo;
    private ParticleSystem stars;

    private int currentTurnSecondsLeft = Globals.MAX_TURN_TIME;

    public bool IsPcPlaying { get { return isPcPlaying; } }
    //public GameSide CurrentTurn { get { return currentTurn; } set { currentTurn = value; } }
    public GameSide PcSide { get { return pcSide; } set { pcSide = value; } }
    public bool IsPaused { get { return isPaused; } }
    public bool IsGameOver { get { return isGameOver; } set { isGameOver = value; } }
    public bool IsDescriptionOpen { get { return isDescriptionOpen; } }

    //private RewardBasedVideoAd reward_based_ad;
    //private int statusAds = 0;
	
	public IEnumerator PostRunningAds(string _param, string _message, int _ads, double _amount)
    {
		Login Udata = new Login();
        WWWForm form = new WWWForm();

        form.AddField("memberID", _param);
        form.AddField("gameID", "19");
        form.AddField("message", _message);
        form.AddField("ads", _ads);
        form.AddField("amount", _amount.ToString());

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "runningAds.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }

    }
	
    IEnumerator postGetProfile(string nickname)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", "19");

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "cekNickName.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
            Udata.bossgame_iduser = jsonObject["id"];
            Udata.bossgame_email = jsonObject["email"];
            Udata.bossgame_username = jsonObject["username"];
            PlayerPrefs.SetString("bossgame_username", Udata.bossgame_username.ToString());
            Udata.bossgame_avatar = jsonObject["photo"];
            PlayerPrefs.SetString("bossgame_avatar", Udata.bossgame_avatar.ToString());
            Udata.bossgame_idvalue = jsonObject["value"];
            Udata.bossgame_valmessage = jsonObject["message"];
            PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);

        }
    }

    IEnumerator postChancePlaying(string nickname, string game_name)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("namaGame", "Zombie Chess War");

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "lifeMinusGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            var jsonObject = JSON.Parse(a);
            Debug.Log(jsonObject["value"] + " - " + jsonObject["message"]);
            if (jsonObject["value"] == 0)
            {
                //this.OnBackToHome();
                Udata.statusPlay = 0;
                //sisa_main_2_message.text = "";
            }
            else if (jsonObject["value"] == 1)
            {
                if (jsonObject["heart"] <= 0)
                {
                    Udata.statusPlay = 0;
                }
                else
                {
                    Udata.statusPlay = 1;
                }
            }
            else if (jsonObject["value"] == 2)
            {
                Udata.statusPlay = 0;
            }
            //StartCoroutine(postGetProfile(PlayerPrefs.GetString("bossgame_nickname").ToString()));
        }
    }
/*
    void InitGoogleAd()
    {
        // #if UNITY_ANDROID
        // 	string app_id = "ca-app-pub-3940256099942544~3347511713";
        // #elif UNITY_IPHONE
        // 	string app_id = "ca-app-pub-3940256099942544~1458002511";
        // #else
        // 	string app_id = "unexpected_platform";
        // #endif

        //production
        //string app_id = "ca-app-pub-2705296754237795~2710084701";
        //sample app id
        string app_id = "ca-app-pub-3940256099942544~3347511713";

        MobileAds.Initialize(app_id);

        this.reward_based_ad = RewardBasedVideoAd.Instance;

        // Called when an ad request has successfully loaded.
        reward_based_ad.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        reward_based_ad.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        reward_based_ad.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        reward_based_ad.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        reward_based_ad.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        reward_based_ad.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        reward_based_ad.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;


        this.RequestRewardBasedVideo();

    }
    private void RequestRewardBasedVideo()
    {
        // #if UNITY_ANDROID
        // 	string ad_unit_id = "ca-app-pub-3940256099942544/5224354917";
        // #elif UNITY_IPHONE
        // 	string ad_unit_id = "ca-app-pub-3940256099942544/1712485313";
        // #else
        // 	string ad_unit_id = "unexpected_platform";
        // #endif

        //production
        //string ad_unit_id = "ca-app-pub-2705296754237795/5144676355";
        //sample unit id
        string ad_unit_id = "ca-app-pub-3940256099942544/5224354917";

        AdRequest request = new AdRequest.Builder().Build();

        this.reward_based_ad.LoadAd(request, ad_unit_id);
    }
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoLoaded", 1, 0.0));
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }
    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoFailedToLoad", 2, 0.0));
    }
    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoOpened", 3, 0.0));
    }
    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoStarted", 4, 0.0));
    }
    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoClosed", 5, 0.0));
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);

        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoRewarded", 6, amount));
    }
    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");

        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoLeftApplication", 7, 0.0));
    }

*/
    void OnGUI()
    {
        /*if (reward_based_ad.IsLoaded())
        {
            if (this.statusAds == 1)
            {
                Debug.Log("show iklan");
                reward_based_ad.Show();
            }

        }*/
    }

    private void Start() {

        //InitGoogleAd();
        //StartCoroutine(postGetProfile(PlayerPrefs.GetString("bossgame_nickname").ToString()));
        Login Udata = new Login();
		Debug.Log(PlayerPrefs.GetString("statusAds"));
        Advertisement.Initialize ("3437563", false);
		PlayerPrefs.SetString("statusAds", "0");
        //this.statusAds = 0;

        Globals.IS_IN_GAME = true;
        cloudInfo = Globals.Instance.UnityObjects["InfoBubble"];
        stars = Globals.Instance.UnityObjects["Stars"].GetComponent<ParticleSystem>();
        if(Globals.IS_SINGLE_PLAYER) {
            Globals.Instance.UnityObjects["ClockDisplay"].SetActive(true);
            //MultiPlayerManager.Instance.gameObject.SetActive(false);
        }
        InitGame();
        //Globals.Instance.UnityObjects["Smoke"].SetActive(false);
        ShutdownScreens();
    }
	
	IEnumerator TestWinLose()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(10);
		WinGame(GameSide.LeftSide);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }
	
	IEnumerator postGetProfile2(string nickname)
    {
		Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", 19);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
        if (www.isNetworkError || www.isHttpError)
        {
			Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API Profile Login");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
			Debug.Log(jsonObject["value"]);
			if(jsonObject["value"]==2){
			PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
			}else{
            
            if (jsonObject["value"] == 1)
            {
				PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
				PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
				Debug.Log("hasil nyawa : "+PlayerPrefs.GetString("bossgame_heart"));
				//SceneManager.LoadScene(13);
            }
			}
        }
    }

    private void InitGame() {
        Screen.SetResolution(1920, 1080, true);
        if (Globals.IS_SINGLE_PLAYER)
        {
            SoldierManager.Instance.InitPcBoard();
            if(PlayerPrefs.GetString("FirstPlayingGame")=="No"){
                Login lg = new Login();
                StartCoroutine(lg.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
                StartCoroutine(postGetProfile2(PlayerPrefs.GetString("bossgame_nickname")));
            }
            PlayerPrefs.SetString("FirstPlayingGame", "No");
            pcSide = (GameSide)UnityEngine.Random.Range(0, 2);
            if (pcSide == GameSide.LeftSide)
            {
                SoldierManager.Instance.FlipSide();
            }
            totalSoldiersLocalSide = SoldierManager.Instance.LocalPlayerList.Count - 1;
            totalSoldiersEnemySide = SoldierManager.Instance.EnemyList.Count - 1;
            CURRENT_TURN = (GameSide)UnityEngine.Random.Range(0, 2);
            SoldierManager.Instance.HideAllSoldiers();
            UpdateStats();
            PassTurn();
            //StartCoroutine(TestWinLose());
            
        }
        else
        {      //game is multiplayer
            if(PlayerPrefs.GetString("FirstPlayingGame")=="No"){
            StartCoroutine(this.postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Zombie Chess War"));
            PlayerPrefs.SetString("bossgame_heart", (int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1).ToString());
            }
            totalSoldiersLocalSide = SoldierManager.Instance.LocalPlayerList.Count - 1;
            totalSoldiersEnemySide = SoldierManager.Instance.EnemyList.Count - 1;
            UpdateStats();

            //Globals.Instance.UnityObjects["ResetBtn"].SetActive(false);
            GameView.DisableButton("ResetWinBtn");

            StartCoroutine(CountTime());
        }
        cloudInfo.SetActive(false);
        stars.Stop();
        UpdateTitles();
        PlayerPrefs.SetString("FirstPlayingGame", "No");
    }

    public IEnumerator CountTime() {
        if(MultiPlayerManager.Instance.IsMyTurn) {
            yield return new WaitForSeconds(1);
            currentTurnSecondsLeft -= 1;
            if(currentTurnSecondsLeft < 11 && currentTurnSecondsLeft > -1) {
                if(currentTurnSecondsLeft % 2 != 0) {
                    SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.ClockTickOne);
                }
                else {
                    SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.ClockTickTwo);
                }
            }
            if(currentTurnSecondsLeft == -1) {
                WinGame(MultiPlayerManager.Instance.PlayerSide == GameSide.LeftSide ? GameSide.RightSide : GameSide.LeftSide, "You didn't make a move!");
                MultiPlayerManager.Instance.SendGameQuit(MultiPlayerManager.Instance.RealUsername + "'s turn time is over");
                yield return null;
            }
            else {
                GameView.SetText("ClockTimeTxt", currentTurnSecondsLeft.ToString());
                yield return CountTime();
            }
        }
        else {
            currentTurnSecondsLeft = Globals.MAX_TURN_TIME;
            GameView.SetText("ClockTimeTxt", currentTurnSecondsLeft.ToString());
            yield return null;
        }
    }

    private void UpdateTitles() {
        if(Globals.IS_SINGLE_PLAYER) {
            GameView.SetText("TitleGame", "Single Player");
            if(pcSide == GameSide.LeftSide) {
                GameView.SetText("LeftSideNameTxt", "COM");
                GameView.SetText("RightSideNameTxt", PlayerPrefs.GetString("Username", "No-Name"));
            }
            else {
                GameView.SetText("LeftSideNameTxt", PlayerPrefs.GetString("Username", "No-Name"));
                GameView.SetText("RightSideNameTxt", "COM");
            }
        }
        else {
            GameView.SetText("TitleGame", "Multi Player");
            if(MultiPlayerManager.Instance.PlayerSide == GameSide.LeftSide) {
                GameView.SetText("LeftSideNameTxt", MultiPlayerManager.Instance.RealUsername);
                GameView.SetText("RightSideNameTxt", MultiPlayerManager.Instance.RealEnemyUsername);
            }
            else {
                GameView.SetText("LeftSideNameTxt", MultiPlayerManager.Instance.RealEnemyUsername);
                GameView.SetText("RightSideNameTxt", MultiPlayerManager.Instance.RealUsername);
            }
        }
    }

    public void PassTurn(Tile oldTile = null, Tile newTile = null) {
        ChangeTurn();
        //FixIntegrity();
        if(Globals.IS_SINGLE_PLAYER) {
            if(CURRENT_TURN == pcSide && !isPcPlaying) {
                isPcPlaying = true;
                StartCoroutine(SoldierManager.Instance.MakeRandomMove());
            }
            else {
                isPcPlaying = false;
            }
        }
        else {  //game is multiplayer
            MultiPlayerManager.Instance.SendMove(oldTile, newTile);
        }

    }

    public void ChangeTurn() {
        CURRENT_TURN = CURRENT_TURN == GameSide.LeftSide ? GameSide.RightSide : GameSide.LeftSide;
        GameView.SetImage("FlagColor", CURRENT_TURN == GameSide.LeftSide ? blueFlagSprite : redFlagSprite);
    }

    public void UpdateStats() {
        if(Globals.IS_SINGLE_PLAYER) {
            if(pcSide == GameSide.RightSide) {
                GameView.SetText("ZombiesLeftText", (SoldierManager.Instance.LocalPlayerList.Count - 1) + " / " + totalSoldiersLocalSide);
                GameView.SetText("ZombiesRightText", (SoldierManager.Instance.EnemyList.Count - 1) + " / " + totalSoldiersEnemySide);
            }
            else {
                GameView.SetText("ZombiesRightText", (SoldierManager.Instance.LocalPlayerList.Count - 1) + " / " + totalSoldiersLocalSide);
                GameView.SetText("ZombiesLeftText", (SoldierManager.Instance.EnemyList.Count - 1) + " / " + totalSoldiersEnemySide);
            }
        }
        else {
            if(MultiPlayerManager.Instance.PlayerSide == GameSide.LeftSide) {
                GameView.SetText("ZombiesLeftText", (SoldierManager.Instance.LocalPlayerList.Count - 1) + " / " + totalSoldiersLocalSide);
                GameView.SetText("ZombiesRightText", (SoldierManager.Instance.EnemyList.Count - 1) + " / " + totalSoldiersEnemySide);
            }
            else {
                GameView.SetText("ZombiesRightText", (SoldierManager.Instance.LocalPlayerList.Count - 1) + " / " + totalSoldiersLocalSide);
                GameView.SetText("ZombiesLeftText", (SoldierManager.Instance.EnemyList.Count - 1) + " / " + totalSoldiersEnemySide);
            }
        }

    }

    public void ShowStars(Flag flag) {
        stars.transform.position = flag.transform.position;
        stars.Play();
    }

    public void CheckWin(GameSide potentialLoserSide) {
        GameSide potentialWinnerSide = potentialLoserSide == GameSide.LeftSide ? GameSide.RightSide : GameSide.LeftSide;
        if(!SoldierManager.Instance.HasZombies(potentialLoserSide)) {
            WinGame(potentialWinnerSide);
        }
    }

    public void WinGame(GameSide winSide, string msg = null) {
        isGameOver = true;
        SoldierManager.Instance.CoverAllSoldiers();
        Globals.Instance.UnityObjects["WinWindow"].SetActive(true);
        if(Globals.IS_SINGLE_PLAYER && pcSide != winSide || !Globals.IS_SINGLE_PLAYER && winSide == MultiPlayerManager.Instance.PlayerSide) {
            GameView.SetText("TitleWinner", "You Won !");
			PlayerPrefs.SetString("statusAds", "0");
            SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.SinglePlayerWin);
			
			if(msg != null) {
				GameView.SetText("MsgWinner", msg);
			}
			CloseInfo();
			SoundManager.Instance.Music.Stop();
			GameView.MakeScreenDark();
			isPaused = true;
			Time.timeScale = 0;           //Pause game here

        }
        else {
			Login Udata = new Login();
			PlayerPrefs.SetString("statusAds", "1");
			//StartCoroutine(this.postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Zombie Chess War"));
			GameView.SetText("TitleWinner", "You Lost !");
			SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.SinglePlayerLose);
			//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
			//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
			
			
			if(msg != null) {
				GameView.SetText("MsgWinner", msg);
			}
			CloseInfo();
			SoundManager.Instance.Music.Stop();
			GameView.MakeScreenDark();
			isPaused = true;
			Time.timeScale = 0;           //Pause game here
			/*PlayerPrefs.SetString("statusAds", "1");
			SceneManager.LoadScene(4);*/
			//this.statusAds = 1;
            /*OnStarted os = new OnStarted();
            StartCoroutine(os.checkInternetConnection((isConnected) => {
                if (!isConnected)
                {
                    SceneManager.LoadScene(3);
                }
                else
                {
                    Login Udata = new Login();
                    //StartCoroutine(this.postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Zombie Chess War"));
                    GameView.SetText("TitleWinner", "You Lost !");
                    SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.SinglePlayerLose);
					//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
					//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
					PlayerPrefs.SetString("statusAds", "1");
					SceneManager.LoadScene(4);
                    //this.statusAds = 1;
                }
            }));*/
        }
        GlobalVar.bannerView.Destroy();
    }

    public void UpdateMusicVolume(float value) {
        SoundManager.Instance.Music.volume = value;
        PlayerPrefs.SetFloat("Music", value);
    }

    public void UpdateSfxVolume(float value) {
        SoundManager.Instance.SFX.volume = value;
        PlayerPrefs.SetFloat("SFX", value);
    }

    public void GoBack() {
        ChangeGameState(prevScreen);
    }

    private void ShutdownScreens() {
        Globals.Instance.UnityObjects["PauseWindow"].SetActive(false);
        Globals.Instance.UnityObjects["WinWindow"].SetActive(false);
        Globals.Instance.UnityObjects["InfoBubble"].SetActive(false);
    }

    public void ChangeGameState(GameScreens newScreen) {
        var unityObjects = Globals.Instance.UnityObjects;

        prevScreen = currentScreen;

        switch(prevScreen) {
            case GameScreens.InGame: unityObjects["PauseWindow"].SetActive(false); break;

            case GameScreens.Pause: unityObjects["ScreenPause"].SetActive(false); break;

            default: break;
        }

        currentScreen = newScreen;
        switch(currentScreen) {
            case GameScreens.InGame:
                ResumeGame();
                break;

            case GameScreens.Pause:
                PauseGame();
                break;

            case GameScreens.Options:
                unityObjects["ScreenOptions"].SetActive(true);
                GameView.SetText("TitlePause", "Settings");
                break;

            default: break;
        }
    }

    private void PauseGame() {
        var unityObjects = Globals.Instance.UnityObjects;
        unityObjects["PauseWindow"].SetActive(true);
        unityObjects["ScreenPause"].SetActive(true);
        GameView.DisableButton("PauseBtn");
        GameView.SetText("TitlePause", "Game Paused");
        GameView.MakeScreenDark();
        isPaused = true;
        if(Globals.IS_SINGLE_PLAYER)
            Time.timeScale = 0;           //Pause game here
    }

    private void ResumeGame() {
        Globals.Instance.UnityObjects["PauseWindow"].SetActive(false);
        GameView.EnableButton("PauseBtn");
        GameView.MakeScreenNormal();
        isPaused = false;
        Time.timeScale = 1;           //Resume game here
    }

    public void QuitGame() {
		
		Login Udata = new Login();
		//StartCoroutine(this.postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Zombie Chess War"));
		//GameView.SetText("TitleWinner", "You Lost !");
		//SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.SinglePlayerLose);
		//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
		//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
		
		if(PlayerPrefs.GetString("statusAds")=="1"){
			SceneManager.LoadScene(4);
		}else{
			SceneManager.LoadScene(1);
		}
		//this.statusAds = 1;
        Time.timeScale = 1;
        Globals.Instance.UnityObjects["PauseWindow"].SetActive(false);
        Destroy(SoldierManager.Instance.gameObject);
        Destroy(SoundManager.Instance.gameObject);
        Destroy(TileManager.Instance.gameObject);
        Destroy(MultiPlayerManager.Instance.gameObject);

        Globals.IS_IN_GAME = false;

        //SceneManager.LoadScene("Main_Scene");
        Initiate.Fade("Main_Scene", GameView.transitionColor, 2f);  //NOT WORKING
    }

    public void ResetMatch() {
        SoldierManager.Instance.StopAllCoroutines();
        StopAllCoroutines();
        SoundManager.Instance.Music.Play();     //stop the music and start over
        TileManager.Instance.ClearTiles();
        SoldierManager.Instance.ClearSoldiers();
        SoldierManager.Instance.LoadStrategy();
        isPcPlaying = false;
        InitGame();
        ResumeGame();
        if(Globals.Instance.UnityObjects["WinWindow"]) {
            GameView.SetText("MsgWinner", "");
            Globals.Instance.UnityObjects["WinWindow"].SetActive(false);
        }
    }

    public IEnumerator DisplayInfo(PlayerSoldier soldier) {
        GameView.SetImage("SoldierImg", soldier.Sprite);
        GameView.SetText("RankInfoTxt", soldier.Rank.ToString());
        GameView.SetText("InfoTitleTxt", soldier.SoldierName);
        GameView.SetText("DescriptionInfoTxt", soldier.Description);
        GameView.SetText("SpecialAbilityTxt", soldier.SpecialAbilityDescription);
        //cloudInfo.transform.position = soldier.transform.position;
        cloudInfo.transform.position = new Vector2(Input.mousePosition.x + (soldier.CurrentSide == GameSide.LeftSide ? 70 : -70), Input.mousePosition.y);
        SoundManager.Instance.SFX.PlayOneShot(SoundManager.Instance.Description);
        isDescriptionOpen = true;
        cloudInfo.SetActive(true);

        yield return new WaitForSeconds(3f);
        CloseInfo();
    }

    public void CloseInfo() {
        if(IsDescriptionOpen) {
            cloudInfo.SetActive(false);
            isDescriptionOpen = false;
        }
    }

    public void FixIntegrity() {
        TileManager.Instance.Fix();
        SoldierManager.Instance.Fix();
    }

    public static Vector3 GetScreenPosition(Transform transform, Canvas canvas, Camera cam) {
        Vector3 pos;
        float width = canvas.GetComponent<RectTransform>().sizeDelta.x;
        float height = canvas.GetComponent<RectTransform>().sizeDelta.y;
        float x = Camera.main.WorldToScreenPoint(transform.position).x / Screen.width;
        float y = Camera.main.WorldToScreenPoint(transform.position).y / Screen.height;
        pos = new Vector3(width * x - width / 2, y * height - height / 2);
        return pos;
    }
}