﻿using System.Collections.Generic;
using UnityEngine;

//public enum GameMode { Edit, SinglePlayer, Multiplayer }

public enum MenuScreens {
    Default, Main, MultiPlayer, Options, Edit
}

public enum GameScreens {
    Default, InGame, Pause, Options
}

public enum GameSide {
    LeftSide, RightSide
}

public class Globals : Singleton<Globals> {

    public const int TOTAL_MONEY = 108;
    public const int MAX_SOLDIERS_FOR_PLAYER = 35; //9 rows on 5 columns = 36. 36 - 1 (flag) = 35
    public const int MIN_SOLDIERS_ALLOWED = 35;
    public const int ROWS = 9;
    public const int COLUMNS = 12;
    public const int MIN_PRICE = 1;
    public const int MAX_BOMBS = 6;
    public const int MAX_SAPPERS = 3;
    public const int MAX_TURN_TIME = 30;
    public const int RANK_OF_SAPPER = 9;
    public const int RANK_OF_COPYCAT = 11;
    public const int RANK_OF_EXPLODER = 6;
    public const string GITHUB_PROFILE_URL = "";
    public const string CV_URL = "";

    public const string API_KEY = "99772d8158424cfb59a37a7d0fb8eb358555b5da9be6f12959265826b015b74d";
    public const string SECRET_KEY = "d21f1eba6d282457c93731e2769e0e741040cd2c4a798716bf5a09ffc800b7c4";

    public static bool IS_IN_GAME;
    public static bool IS_SINGLE_PLAYER = true;

    public MenuScreens currentScreen;

    private Dictionary<string, GameObject> unityObjects;

    public Dictionary<string, GameObject> UnityObjects { get { return unityObjects; } }

    private void Awake() {
        Init();
    }

    private void Init() {
        unityObjects = new Dictionary<string, GameObject>();
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("UnityObject");
        foreach(var ob in gameObjects) {
            //if(!unityObjects.ContainsKey(ob.name))
            unityObjects.Add(ob.name, ob);
            //Debug.Log(ob.name);
        }

        //PlayerPrefs.DeleteAll();    //For testing
    }

    public Dictionary<string, SoldierBtn> GetAllSoldierBtns() {
        var soldierBtns = FindObjectsOfType<SoldierBtn>();
        Dictionary<string, SoldierBtn> soldierButtonsDic = new Dictionary<string, SoldierBtn>();
        foreach(var btn in soldierBtns) {
            soldierButtonsDic.Add(btn.SoldierObject.name, btn);
        }
        return soldierButtonsDic;
    }
}