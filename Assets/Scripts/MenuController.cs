﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController: MonoBehaviour {

    private Slider moneySlider, musicSlider, sfxSlider;
    private SoundManager soundManager;
    private InputField usernameField;

    private void Start() {

        moneySlider = Globals.Instance.UnityObjects["MoneySlider"].GetComponent<Slider>();
        musicSlider = Globals.Instance.UnityObjects["MusicSlider"].GetComponent<Slider>();
        sfxSlider = Globals.Instance.UnityObjects["SfxSlider"].GetComponent<Slider>();
        usernameField = Globals.Instance.UnityObjects["Input_Username"].GetComponent<InputField>();

        soundManager = SoundManager.Instance;

        moneySlider.value = PlayerPrefs.GetInt("MoneyBet", 2);
        musicSlider.value = PlayerPrefs.GetFloat("Music", 1);
        sfxSlider.value = PlayerPrefs.GetFloat("SFX", 1);
        usernameField.text = PlayerPrefs.GetString("Username", "No-Name");
        usernameField.readOnly = true;
		PlayerPrefs.SetString("statusAds", "0");
    }


    public void PlayerSelectedInEditMode(SoldierBtn soldierSelected) {
        StrategyEditor.Instance.SelectedSoldier(soldierSelected);
    }

    public void SaveChangesInEditMode() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.ChangeMenuState(MenuScreens.Main);
        MenuLogic.Instance.SaveStrategy();
    }

    public void StartSingleGame() {
		if(PlayerPrefs.GetInt("bossgame_value") == 2){
			PlayerPrefs.SetInt("bossgame_value", 1);
			Destroy(SoldierManager.Instance.gameObject);
			Destroy(SoundManager.Instance.gameObject);
			Destroy(TileManager.Instance.gameObject);
			Destroy(MultiPlayerManager.Instance.gameObject);
			SceneManager.LoadScene(0);
		}else{
			MenuLogic.Instance.StartGame(true);
		}
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
    }

    public void StartMultiplayerGame() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.StartGame(false);
    }

    public void Multiplayer() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
		Globals.Instance.UnityObjects["ErrorWindow"].SetActive(true);
		GameView.SetText("TitleMenu", "Warning");
		GameView.SetText("ErrorTxt", "Sorry , This Feature Under Maintenance .. Thank you");
        //MenuLogic.Instance.ChangeMenuState(MenuScreens.MultiPlayer);
    }

    public void EditMode() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.ChangeMenuState(MenuScreens.Edit);
    }

    public void Options() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.ChangeMenuState(MenuScreens.Options);
    }

    public void QuitGame() {
        Login Udata = new Login();
        Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
        StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
        StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "19", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Application.Quit();
    }

    public void Back() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.GoBack();
    }

    public void MoveSfxSlider() {
        MenuLogic.Instance.UpdateSfxVolume(sfxSlider.value);
    }

    public void MoveMusicSlider() {
        MenuLogic.Instance.UpdateMusicVolume(musicSlider.value);
    }

    public void MoveMoneySlider() {
        MenuLogic.Instance.UpdateMoneySliderTxt(moneySlider.value);
    }

    public void StartGithub() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.OpenGithub();
    }

    public void StartCV() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.OpenCV();
    }

    public void ChangeUsername() {
        MenuLogic.Instance.UpdateUsername(usernameField.text);
    }

    public void CancelConnection() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MultiPlayerManager.Instance.Disconnect();
    }

    public void ConfirmError() {
        soundManager.SFX.PlayOneShot(soundManager.ButtonPress);
        MenuLogic.Instance.ConfirmError();
    }
}